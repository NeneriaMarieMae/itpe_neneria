import React, { Component } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native'; 

export default class ViewScreen extends Component {
    render() {
      return (
        <View
          style={{
            flexDirection: 'row',
            height: 100,
            padding: 20,
          }}>
          <View style={{backgroundColor: 'blue', flex: 0.3}} />
          <View style={{backgroundColor: 'skyblue', flex: 0.5}} />
          <Text>Hello World!</Text>
          <Button style={styles.space}
                title="Home"
                onPress={() => this.props.navigation.navigate('Home')}
            />
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 20,
      margin: 20,
      textAlign: 'center',
    },
    space: {
      margin: 20,
    }
  });

