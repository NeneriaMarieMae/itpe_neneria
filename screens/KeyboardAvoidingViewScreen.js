import React, { Component } from 'react';
import { Text, Button, TextInput, KeyboardAvoidingView, View, StyleSheet } from 'react-native';

export default class KeyboardAvoidingViewScreen extends Component {
  state = {
    email: '',
  };
  
  render() {
    return (
      <View style={styles.container}>
      <View style={{height: 50, backgroundColor: 'lightblue'}}/>
        <KeyboardAvoidingView behavior="padding" style={styles.form}>
          <TextInput
            style={styles.input}
            value={this.state.email}
            onChangeText={email => this.setState({email})}
            ref={ref => {this._emailInput = ref}}
            placeholder="email@example.com"
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="email-address"
            returnKeyType="send"
            onSubmitEditing={this._submit}
            // keyboardVerticalOffset={50}
            blurOnSubmit={true}
          />
          <View>
            <Button title="Sign Up" onPress={this._submit} />
            <Text style={styles.legal}>
              Some important legal fine print here
            </Text>
          </View>
        </KeyboardAvoidingView>
        <Button
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
  
  _submit = () => {
    alert(`Confirmation email has been sent to ${this.state.email}`);
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'ligthblue',
  },
  input: {
    margin: 20,
    marginBottom: 0,
    height: 34,
    paddingHorizontal: 10,
    borderRadius: 4,
    borderColor: '#ccc',
    borderWidth: 1,
    fontSize: 16,
  },
  legal: {
    margin: 10,
    color: '#333',
    fontSize: 12,
    textAlign: 'center',
  },
  form: {
    flex: 1,
    justifyContent: 'space-between',
  },
});
